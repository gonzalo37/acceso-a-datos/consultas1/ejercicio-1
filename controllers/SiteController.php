<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Maillot;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades sin repetidos",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
        
        
    }
    public function actionOtraconsulta1(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta2(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("edad")->distinct()
                ->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
        ]);
        }
        public function actionOtraconsulta2(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct edad) from ciclista where nomequipo='Artiach'")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
        ]);
        
        }
        
         public function actionConsulta3(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("edad")->distinct()
                ->where("nomequipo='Artiach'OR nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'OR nomequipo='Amore Vita'",
        ]);
        }
        
        public function actionOtraconsulta3(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct edad) from ciclista where nomequipo='Artiach'or nomequipo='Amore Vita'")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
        ]);
        
        }
        
        public function actionConsulta4(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("dorsal")->distinct()
                ->where("edad<25 OR edad>30"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad<25 OR edad>30",
        ]);
        }
        
        public function actionOtraconsulta4(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct dorsal) from ciclista where edad<25 or edad>30")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad<25 OR edad>30",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE edad<25 OR edad>30",
        ]);
        
        }
        
         public function actionConsulta5(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("dorsal")->distinct()
                ->where("edad BETWEEN 28 AND 32 AND nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
        ]);
        }
        
        public function actionOtraconsulta5(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct dorsal) from ciclista where edad between 28 and 32 and nomequipo='Banesto'")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
        ]);
        
        }
    public function actionConsulta6(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("nombre")->distinct()
                ->where("LENGTH(nombre)>8"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE LENGTH(nombre)>8",
        ]);
        }
        
        public function actionOtraconsulta6(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct nombre) from ciclista where length(nombre)>8")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT nombre FROM ciclista WHERE LENGTH(nombre)>8",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE LENGTH(nombre)>8",
        ]);
        
        }
        public function actionConsulta7(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("nombre,dorsal,UPPER(nombre) AS mayusculas")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre','dorsal','mayusculas'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT nombre,dorsal,UPPER(nombre) AS mayusculas FROM ciclista",
        ]);
        }
        
        public function actionOtraconsulta7(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct nombre,dorsal,upper(nombre) as mayusculas) from ciclista")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT nombre,dorsal,UPPER(nombre) AS mayusculas FROM ciclista",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre','dorsal','mayusculas'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT nombre,dorsal,UPPER(nombre) AS mayusculas FROM ciclista",
        ]);
        
        }
        
         public function actionConsulta8(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Maillot::find()->select("código")->distinct()
                ->where("color='amarillo'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['código'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT código FROM maillot WHERE color='amarillo'",
        ]);
        }
        
        public function actionOtraconsulta8(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct código) from maillot where color='amarillo'")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT código FROM maillot WHERE color='amarillo'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['código'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT código FROM maillot WHERE color='amarillo'",
        ]);
        
        }
        
     public function actionConsulta9(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Puerto::find()->select("nompuerto")->distinct()
                ->where("altura>1500"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500",
        ]);
        }
        
        public function actionOtraconsulta9(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct nompuerto) from puerto where altura>1500")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500",
        ]);
        
        }
        
        public function actionConsulta10(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Puerto::find()->select("dorsal")->distinct()
                ->where("pendiente>8 OR altura BETWEEN 1800 AND 3000"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000 ",
        ]);
        }
        
        public function actionOtraconsulta10(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct dorsal) from puerto where pendiente>8 or altura between 1800 and 3000")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
        
        }
        
        public function actionConsulta11(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Puerto::find()->select("dorsal")->distinct()
                ->where("pendiente>8 AND altura BETWEEN 1800 AND 3000"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000 ",
        ]);
        }
        
        public function actionOtraconsulta11(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct dorsal) from puerto where pendiente>8 and altura between 1800 and 3000")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura BETWEEN 1800 AND 3000",
        ]);
        
        }
        

}